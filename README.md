The `cellprops` package
=======================

This package reworks the internals of `tabular`, `array`, and similar
constructs, and adds a `\cellprops` command accepting CSS-like selectors and
properties.  It depends on `mdwtab`, `xcolor` and of course `expl3` and
`xparse`.

You can use most constructs of `mdwtab`, but some too hackish usage might
alter the function of cellprops.

Look at the PDF documentation to see examples and detailed usage.



Copyright (C) 2016-2019  Julien "_FrnchFrgg_" RIVAUD

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
